# imooc前端

#### 介绍
imooc的前端，包括首页展示、热门课程展示、名师展示、阿里云视频播放等

#### 软件架构
采用nuxt.js框架搭建，使用服务器端渲染提高SEO搜索排名


#### 安装教程

1、克隆项目 git clone https://gitee.com/pluto_gitee/imooc-front-end.git

2、引入依赖 npm install

3、运行 npm run dev
